---
title: "Projects"
slug: "projects"
description: "Projects made by Caleb Marshall"
date: 2021-10-20T22:45:03-05:00
type: "page"
layout: "partials"
---
# Gladupe

A snake game written in C. You can get it from the
[snap store](https://snapcraft.io/gladupe/) or [play it
online](/games/gladupe/). If you would like to play it on a different platform
(e.g., Windows) send me an email and I'll see what I can do.

# Snowballz 2

A multiplayer RTS featuring a penguin snowball fight. It is still a work in
progress, but you can check out the [source
code](https://gitlab.com/cajomar/snowballz2/).

# Curen

An OpenGL voxel renderer. You can either build the [source
code](https://gitlab.com/cajomar/curen/) or [play it online](/games/curen/).

# Tatlap

Texture atlas packer, for packing many small rectangular images into a large
one. You can [install it with cargo](https://crates.io/crates/tatlap/).
